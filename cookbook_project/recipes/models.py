#encoding: utf-8
"""Module docs.
"""
from django.contrib.auth.models import User
from django.db import models
from django.core.urlresolvers import reverse
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _



# Crerate your models here.

class Category(models.Model):
    """Category model.
    dieser Text wird von Django/Python gesammelt (von Docstring) und angezeigt

    """
    name = models.CharField(_('Name'), max_length=100)
    slug = models.SlugField(unique=True) #ist ein Standard-Charfield, wird auch für die URL benutzt
    description = models.TextField(blank=True, verbose_name=_('Description')) # verbose_name ist eine andere Schreibweise für 'Name', beim Beziehung zu andere Tabellen soll man verbose_name benutzen.

    class Meta: # Ab Django 1.7 gibt es auch Meta-Daten für die APP selbst.
        verbose_name = 'Kategorie'
        verbose_name_plural = 'Kategorien'

    def __unicode__(self):
        return self.name

class Recipe(models.Model):
    DIFFICULTY_EASY = 1
    DIFFICULTY_MEDIUM = 2
    DIFFICULTY_HARD = 3
    DIFFICULTIES = (
        (DIFFICULTY_EASY, _('leicht')),
        (DIFFICULTY_MEDIUM, _('normal')),
        (DIFFICULTY_HARD, _('schwer')),
    )

    title = models.CharField(_('Titel'), max_length=255)
    slug = models.SlugField(unique=True)
    ingredients = models.TextField('Zutaten', help_text='Eine Zutat pro Zeile eingeben')
    preparation = models.TextField('Zubereitung')
    time_for_preparation = models.IntegerField(_('Zeit zur Zubereitung'),
                    help_text='Zeit in Minuten angeben', blank=True, null=True)
    number_of_portions = models.PositiveIntegerField(_('Anzahl der Portionen'))
    difficulty = models.SmallIntegerField(_('Schwirigkeitsgrad'), choices=DIFFICULTIES,
                    default=DIFFICULTY_MEDIUM)
    category = models.ManyToManyField(Category, verbose_name='Kategorien') # an der Stellen muss man verbose_name benutzen.
    author = models.ForeignKey(User, verbose_name='Autor')
    photo = models.ImageField(upload_to='recipes', verbose_name=_('Foto')) # an Stelle von 'recipes' kann man eine Funktion aufrufen die ein Str. zurückliefert.
    date_created = models.DateTimeField(editable=False) # das wird ist nicht edetierbar
    date_updated = models.DateTimeField(editable=False)
    is_active = models.BooleanField(_('Freigeschaltet'), default=True)

    class Meta:
        verbose_name = 'Rezept'
        verbose_name_plural = 'Rezepte'
        ordering = ['-date_created']

    def __unicode__(self):
        return self.title

    # wir wollen erst die Attribute setzen und dann speichern, deshalb überschreiben wir die save-Methode
    def save(self, *args, **kwargs): #argumente und Keyword-argumente
        if not self.id:
            self.date_created = now()
        self.date_updated = now()
        super(Recipe, self).save(*args, **kwargs) # kuemmert sich um die Austausch mit der Elternklasse

    def get_absolute_url(self): #dies gibt der URL vom Objekt (einzelnen Rezepte)
        return reverse('recipes_recipe_detail', kwargs={'slug':self.slug})