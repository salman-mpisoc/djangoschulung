#encoding: UTF8
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, \
    render

from .models import Recipe

def index(request):
    #import pdb; pdb.set_trace() #debuggen interactive s. script Seite 54
    recipes = Recipe.objects.all()
    #assert False, recipes
    return render(request, 'recipes/index.html', #request wird mitgegeben damit die Request-Informationen
        {'object_list': recipes})               # (wie z.B. angemeldeter User, session ... an die Template weitergegeben werden

def detail(request, slug):
    recipe = get_object_or_404(Recipe, slug=slug)
    return render(request,'recipes/detail.html',
        {'object': recipe})

@login_required
def secret(request):
    return render(request, 'recipes/secret.html')