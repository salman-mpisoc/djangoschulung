from tastypie.resources import ModelResource

from .models import Recipe


class RecipeResource(ModelResource):
    class Meta:
        queryset = Recipe.objects.all() #.filter(is_active=True) #dann liefert die API nur aktive rezepte
        resource_name = 'recipe'