from django.contrib import admin

# Register your models here.
from .models import Category, Recipe # .models --> relative import, damit ist man sicher, dass die Klassen aus dem gleichen Verzeichnis importiert werden

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug':['name']} # es wird ein javascript aufgerufen --> alles was man in dem Feld "name" eintippt wird an das Feld slug weitergegeben
                                            # davor wird es gefiltert, z.B. Umlaute usw...
class RecipeAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug':['title']}
    list_display = ('title','author', 'is_active', 'number_of_portions')
    list_editable = ('is_active',)
    search_fields = ['preparation', 'title', 'author__username']
    list_filter = ('number_of_portions', 'difficulty')
    def queryset(self, request):
        qs = super(RecipeAdmin, self).queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(author=request.user)


admin.site.register(Category, CategoryAdmin) # site bezieht sich auf die aktuelle Webseite
admin.site.register(Recipe, RecipeAdmin)