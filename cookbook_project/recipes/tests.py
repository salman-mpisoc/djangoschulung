#encoding: UTF-8
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.test import TestCase
from django.utils import timezone
from freezegun import freeze_time

from .models import Recipe

# Create your tests here.

class RecipeSaveTest(TestCase):
    title = u'Erbsensuppe mit Würstchen'
    number_of_portions = 4

    def setUp(self): #diese Methode wird vor jedem Test aufgerufen
        self.author = User.objects.create_user('testuser','test@example.com','testuser')

    def testDateCreatedAutotest(self):
        with freeze_time('2014-01-01 12:00:01'):
            recipe = Recipe.objects.create(
                title=self.title,
                slug = slugify(self.title),
                number_of_portions = self.number_of_portions,
                author = self.author
            )
            self.assertEqual(recipe.date_created, timezone.now())

    def testUnicode(self):
        recipe = Recipe(title=self.title)
        self.assertEqual(unicode(recipe), self.title)