from django.conf.urls import patterns, include, url

from .api import RecipeResource

recipe_resource = RecipeResource()

urlpatterns = patterns('recipes.views',
    url(r'^api/', include(recipe_resource.urls)),
    url(r'^recipe/(?P<slug>[-\w]+)/$', 'detail', name='recipes_recipe_detail'),
    url(r'^secret/$', 'secret', name='recipes_secret'),
    url(r'^$', 'index', name='recipes_recipe_index'),
)
